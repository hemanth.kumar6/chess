package piece;

import board.Board;
import board.Spot;

public class Knight extends Piece {
    public Knight(PieceColor pieceColor) {
        super(pieceColor);
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) {
        if (end.getPiece() != null && end.getPiece().isWhite() == this.isWhite()) return false;

        int xPositionDifference = Math.abs(start.getXPosition() - end.getXPosition());
        int yPositionDifference = Math.abs(start.getYPosition() - end.getYPosition());

        return xPositionDifference * yPositionDifference == 2;
    }
}
