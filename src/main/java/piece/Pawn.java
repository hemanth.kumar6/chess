package piece;

import board.Board;
import board.Spot;

public class Pawn extends Piece{
    public Pawn(PieceColor pieceColor) {
        super(pieceColor);
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) {
        return false;
    }
}
