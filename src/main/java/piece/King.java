package piece;

import board.Board;
import board.Spot;

public class King extends Piece {
    public King(PieceColor pieceColor) {
        super(pieceColor);
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) {
        if (end.getPiece() != null && end.getPiece().isWhite() == this.isWhite()) {
            return false;
        }

        int xDiffPosition = Math.abs(start.getXPosition() - end.getXPosition());
        int yDiffPosition = Math.abs(start.getYPosition() - end.getYPosition());

        return xDiffPosition + yDiffPosition == 1;
    }
}
