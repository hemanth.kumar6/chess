package board;

import exceptions.ChessBoardIndexOutOfBoundException;
import piece.*;

public class Board {
    private Spot[][] chessBoardPosition;

    public Board() {
        this.initChessBoard();
    }

    public Spot getSpotAt(int xPos, int yPos) throws ChessBoardIndexOutOfBoundException {
        checkSpotIndex(xPos, yPos);
        return chessBoardPosition[xPos][yPos];
    }

    public void testSetSpotAt(int xPos, int yPos, Spot piece) throws ChessBoardIndexOutOfBoundException {
        checkSpotIndex(xPos, yPos);
        chessBoardPosition[xPos][yPos] = piece;
    }

    private void initChessBoard() {
        chessBoardPosition = new Spot[8][8];
        for (int yPtr = 0; yPtr <= 7; yPtr++) {
            chessBoardPosition[0][yPtr] = addNonPawnPiecesAt(0, yPtr, PieceColor.WHITE);
            chessBoardPosition[1][yPtr] = new Spot(1, yPtr, new Pawn(PieceColor.WHITE));
            chessBoardPosition[6][yPtr] = new Spot(6, yPtr, new Pawn(PieceColor.BLACK));
            chessBoardPosition[7][yPtr] = addNonPawnPiecesAt(7, yPtr, PieceColor.BLACK);
        }
        for (int xPtr = 2; xPtr < 6; xPtr++) {
            for (int yPtr = 0; yPtr < 8; yPtr++) {
                chessBoardPosition[xPtr][yPtr] = new Spot(xPtr, yPtr, null);
            }
        }
    }

    private Spot addNonPawnPiecesAt(int x, int y, PieceColor color) {
        Spot nonPawnPieceSpot = null;
        switch (y){
            case 0:
            case 7:
                nonPawnPieceSpot =  new Spot(x, y, new Rook(color));
                break;
            case 1:
            case 6:
                nonPawnPieceSpot =  new Spot(x, y, new Knight(color));
                break;
            case 2:
            case 5:
                nonPawnPieceSpot =  new Spot(x, y, new Bishop(color));
                break;
            case 3:
                nonPawnPieceSpot =  new Spot(x, y, new Queen(color));
                break;
            case 4:
                nonPawnPieceSpot =  new Spot(x, y, new King(color));
                break;
        }
        return nonPawnPieceSpot;
    }
    private void checkSpotIndex(int xPos, int yPos) throws ChessBoardIndexOutOfBoundException {
        if (xPos > 7 || xPos < 0 || yPos > 7 || yPos < 0) {
            throw new ChessBoardIndexOutOfBoundException("Index Out of Bound Exception");
        }
    }

}


