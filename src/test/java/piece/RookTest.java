package piece;

import board.Board;
import board.Spot;
import exceptions.ChessBoardIndexOutOfBoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RookTest {

    @Test
    void shouldReturnFalseWhenEndSpotIsAValidSpotButAnotherPieceIsInBetweenStartAndEndSpot() throws ChessBoardIndexOutOfBoundException {
        Board board = new Board();
        Spot startSpot = board.getSpotAt(7, 0);
        Spot endSpot = board.getSpotAt(4, 0);

        boolean canRookMove = startSpot.getPiece().canMove(board, startSpot, endSpot);

        assertFalse(canRookMove);
    }
    @Test
    void shouldReturnTrueWhenEndSpotIsValidAndVerticalPathIsFree() throws ChessBoardIndexOutOfBoundException {
        Board board = new Board();
        Spot startSpot = board.getSpotAt(7, 0);
        Spot endSpot = board.getSpotAt(5, 0);
        board.testSetSpotAt(6, 0, new Spot(6,0 ,null));

        boolean canRookMove = startSpot.getPiece().canMove(board, startSpot, endSpot);

        assertTrue(canRookMove);
    }

    @Test
    void shouldReturnTrueWhenEndSpotIsValidAndHorizontalPathIsFree() throws ChessBoardIndexOutOfBoundException {
        Board board = new Board();
        Rook rook = new Rook(PieceColor.BLACK);
        board.testSetSpotAt(4, 0, new Spot(4, 0, rook));
        Spot startSpot = board.getSpotAt(4, 0);
        Spot endSpot = board.getSpotAt(4, 7);

        boolean canRookMove = startSpot.getPiece().canMove(board, startSpot, endSpot);

        assertTrue(canRookMove);
    }

    @Test
    void shouldReturnTrueWhenEndSpotIsValidAndHorizontalPathFromRightToLeftIsFree() throws ChessBoardIndexOutOfBoundException {
        Board board = new Board();
        Rook rook = new Rook(PieceColor.BLACK);
        board.testSetSpotAt(4, 7, new Spot(4, 7, rook));
        Spot startSpot = board.getSpotAt(4, 7);
        Spot endSpot = board.getSpotAt(4, 0);

        boolean canRookMove = startSpot.getPiece().canMove(board, startSpot, endSpot);

        assertTrue(canRookMove);
    }

}