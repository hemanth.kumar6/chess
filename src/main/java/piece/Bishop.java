package piece;

import board.Board;
import board.Spot;
import exceptions.ChessBoardIndexOutOfBoundException;


public class Bishop extends Piece {

    public Bishop(PieceColor pieceColor) {
        super(pieceColor);
    }

    private boolean isPathFree(Board board, int xStartPosition, int yStartPosition, int xEndPosition, int yEndPosition) throws ChessBoardIndexOutOfBoundException {
        int xOperation;
        int yOperation;

        while (xStartPosition != xEndPosition && yStartPosition != yEndPosition) {
            if (xStartPosition > xEndPosition && yStartPosition > yEndPosition) {
                xOperation = -1;
                yOperation = -1;
            } else if (xStartPosition > xEndPosition) {
                xOperation = -1;
                yOperation = 1;
            } else if (yStartPosition < yEndPosition) {
                xOperation = 1;
                yOperation = 1;
            } else {
                xOperation = 1;
                yOperation = -1;
            }
            xStartPosition += xOperation;
            yStartPosition += yOperation;

            if (board.getSpotAt(xStartPosition, yStartPosition).getPiece() != null) return false;
        }
        return true;
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) throws ChessBoardIndexOutOfBoundException {
        if (end.getPiece() != null && end.getPiece().isWhite() == this.isWhite()) return false;

        int xPositionDifference = Math.abs(start.getXPosition() - end.getXPosition());
        int yPositionDifference = Math.abs(start.getYPosition() - end.getYPosition());
        if (xPositionDifference - yPositionDifference != 0) return false;

        return this.isPathFree(board, start.getXPosition(), start.getYPosition(), end.getXPosition(), end.getYPosition());

    }
}
