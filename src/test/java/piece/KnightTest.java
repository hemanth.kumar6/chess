package piece;

import board.Board;
import board.Spot;
import exceptions.ChessBoardIndexOutOfBoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KnightTest {
    @Test
    void shouldReturnTrueWhenBlackKnightHasAVacantAndValidSpot() {
        PieceColor pieceColor = PieceColor.BLACK;
        Knight knight = new Knight(pieceColor);
        Board board = new Board();
        Spot startSpot = new Spot(7, 1, knight);
        Spot validEndSpot = new Spot(5, 0, null);

        boolean canKnightMove = knight.canMove(board, startSpot, validEndSpot);

        assertTrue(canKnightMove);
    }

    @Test
    void shouldReturnTrueWhenWhiteKnightHasAVacantAndValidSpot() throws ChessBoardIndexOutOfBoundException {
        Board board = new Board();
        Spot startSpot = board.getSpotAt(0, 1);
        Spot validEndSpot = board.getSpotAt(2, 2);

        boolean canKnightMove = board.getSpotAt(0, 1).getPiece().canMove(board, startSpot, validEndSpot);

        assertTrue(canKnightMove);
    }
}