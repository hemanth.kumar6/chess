package piece;

import java.util.Objects;
import board.Board;
import board.Spot;
import exceptions.ChessBoardIndexOutOfBoundException;


public abstract class Piece {
    private final boolean isWhite;
    private boolean isKilled;

    public Piece(PieceColor pieceColor) {
        this.isWhite = (pieceColor == PieceColor.WHITE);
    }

    public boolean isKilled() {
        return isKilled;
    }

    public void setKilled(boolean killed) {
        isKilled = killed;
    }

    public boolean isWhite() {
        return this.isWhite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piece piece = (Piece) o;
        return isWhite == piece.isWhite;
    }

    @Override
    public int hashCode() {
        return Objects.hash(isWhite);
    }

    public abstract boolean canMove(Board board, Spot start, Spot end) throws ChessBoardIndexOutOfBoundException;
}
