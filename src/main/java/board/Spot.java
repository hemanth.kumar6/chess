package board;

import piece.Piece;

import java.util.Objects;

public class Spot {
    private final Piece piece;
    private final int xPosition;
    private final int yPosition;

    public Spot(int xPosition, int yPosition, Piece piece) {
        this.piece = piece;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public Piece getPiece() {
        return this.piece;
    }

    public int getXPosition() {
        return xPosition;
    }

    public int getYPosition() {
        return yPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Spot spot = (Spot) o;
        System.out.println("Hello");
        return piece.equals(spot.piece);
    }

    @Override
    public int hashCode() {
        return Objects.hash(piece);
    }
}
