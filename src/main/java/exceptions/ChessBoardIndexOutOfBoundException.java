package exceptions;

public class ChessBoardIndexOutOfBoundException extends Exception{
    public ChessBoardIndexOutOfBoundException(String message){
        super(message);
    }
}
