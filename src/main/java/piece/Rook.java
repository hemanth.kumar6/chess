package piece;

import board.Board;
import board.Spot;
import exceptions.ChessBoardIndexOutOfBoundException;

public class Rook extends Piece {
    public Rook(PieceColor pieceColor) {
        super(pieceColor);
    }

    private boolean isPathFree(Board board, Spot start, Spot end) throws ChessBoardIndexOutOfBoundException {
        int xStartPosition = start.getXPosition();
        int yStartPosition = start.getYPosition();
        int xEndPosition = end.getXPosition();
        int yEndPosition = end.getYPosition();

        int xOperand = 0;
        int yOperand = 0;

        while (xStartPosition != xEndPosition || yStartPosition != yEndPosition) {
            if (xStartPosition > xEndPosition) {
                xOperand = -1;
            } else if (xStartPosition < xEndPosition) {
                xOperand = 1;
            } else if (yStartPosition > yEndPosition) {
                yOperand = -1;
            } else {
                yOperand = 1;
            }

            xStartPosition += xOperand;
            yStartPosition += yOperand;

            xOperand = 0;
            yOperand = 0;

            if (board.getSpotAt(xStartPosition, yStartPosition).getPiece() != null) return false;
        }
        return true;
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) throws ChessBoardIndexOutOfBoundException {
        if (end.getPiece() != null && end.getPiece().isWhite() == this.isWhite()) return false;

        int xPositionDifference = Math.abs(start.getXPosition() - end.getXPosition());
        int yPositionDifference = Math.abs(start.getYPosition() - end.getYPosition());

        if (!(xPositionDifference == 0 || yPositionDifference == 0)) return false;

        return isPathFree(board, start, end);
    }
}
