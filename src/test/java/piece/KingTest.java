package piece;

import board.Board;
import board.Spot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KingTest {

    @Test
    void shouldAssertTrueWhenAWhiteKingPieceIsCreated() {
        PieceColor pieceColor = PieceColor.WHITE;

        King king = new King(pieceColor);

        assertTrue(king.isWhite());
    }

    @Test
    void shouldReturnFalseWhenEndSpotHasWhitePawnPiece() {
        PieceColor pieceColor = PieceColor.WHITE;
        King king = new King(pieceColor);
        Pawn pawn = new Pawn(pieceColor);
        Board board = new Board();
        Spot startSpot = new Spot(0, 4, king);
        Spot invalidEndSpot = new Spot(1, 4, pawn);

        boolean canKingMove = king.canMove(board, startSpot, invalidEndSpot);

        assertFalse(canKingMove);
    }
}