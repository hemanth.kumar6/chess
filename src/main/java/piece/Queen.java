package piece;

import board.Board;
import board.Spot;

public class Queen extends Piece{
    public Queen(PieceColor pieceColor) {
        super(pieceColor);
    }

    @Override
    public boolean canMove(Board board, Spot start, Spot end) {
        return false;
    }
}
