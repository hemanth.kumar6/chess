import board.Spot;
import org.junit.jupiter.api.Test;
import piece.King;
import piece.PieceColor;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpotTest {
    @Test
    void shouldBeAbleToAddKingPieceToASpot() {
        PieceColor pieceColor = PieceColor.WHITE;
        King king = new King(pieceColor);
        int xPosition = 0;
        int yPosition = 0;

        Spot spot = new Spot(xPosition, yPosition, king);

        assertEquals(spot.getPiece(), king);
    }

    @Test
    void shouldReturnCurrentXPositionOfKingPieceFromTheSpot() {
        PieceColor pieceColor = PieceColor.BLACK;
        King king = new King(pieceColor);
        int xPosition = 8;
        int yPosition = 8;

        Spot spot = new Spot(xPosition, yPosition, king);

        int expectedXPos = 8;
        assertEquals(expectedXPos, spot.getXPosition());
    }
}
