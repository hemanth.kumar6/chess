package piece;

import board.Board;
import board.Spot;
import exceptions.ChessBoardIndexOutOfBoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BishopTest {

    @Test
    void shouldReturnFalseWhenBlackBishopHasANonValidSpot() throws ChessBoardIndexOutOfBoundException {
        PieceColor pieceColor = PieceColor.BLACK;
        Bishop bishop = new Bishop(pieceColor);
        Board board = new Board();
        Spot startSpot = new Spot(7, 5, bishop);
        Spot endSpot = new Spot(2, 0, null);

        boolean canBishopMove = bishop.canMove(board, startSpot, endSpot);

        assertFalse(canBishopMove);
    }

    @Test
    void shouldReturnTrueWhenBlackBishopHasAValidSpot() throws ChessBoardIndexOutOfBoundException {
        PieceColor pieceColor = PieceColor.BLACK;
        Bishop bishop = new Bishop(pieceColor);
        Board board = new Board();
        board.testSetSpotAt(6, 4, new Spot(8, 8, null));
        Spot startSpot = new Spot(7, 5, bishop);
        Spot endSpot = new Spot(2, 0, null);

        boolean canBishopMove = bishop.canMove(board, startSpot, endSpot);

        assertTrue(canBishopMove);
    }

    @Test
    void shouldReturnFalseWhenWhiteBishopHasANonValidSpot() throws ChessBoardIndexOutOfBoundException {
        PieceColor pieceColor = PieceColor.WHITE;
        Bishop bishop = new Bishop(pieceColor);
        Board board = new Board();
        Spot startSpot = new Spot(0, 2, bishop);
        Spot endSpot = new Spot(2, 0, null);

        boolean canBishopMove = bishop.canMove(board, startSpot, endSpot);

        assertFalse(canBishopMove);
    }

    @Test
    void shouldReturnTrueWhenWhiteBishopHasAValidSpot() throws ChessBoardIndexOutOfBoundException {
        PieceColor pieceColor = PieceColor.WHITE;
        Bishop bishop = new Bishop(pieceColor);
        Board board = new Board();
        board.testSetSpotAt(1, 3, new Spot(8, 8, null));
        Spot startSpot = new Spot(0, 2, bishop);
        Spot endSpot = new Spot(5, 7, null);

        boolean canBishopMove = bishop.canMove(board, startSpot, endSpot);

        assertTrue(canBishopMove);
    }
}