import exceptions.ChessBoardIndexOutOfBoundException;

import org.junit.jupiter.api.Test;

import piece.Pawn;
import piece.Piece;
import piece.PieceColor;
import piece.Rook;

import board.Board;
import board.Spot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class BoardTest {
    @Test
    void shouldBeAbleToGetTheCurrentSpotOfRookFromTheBoard() throws ChessBoardIndexOutOfBoundException {
        Board board = new Board();
        int xPos = 0;
        int yPos = 0;

        Spot expectedSpot = board.getSpotAt(xPos, yPos);

        Spot actualSpot = new Spot(xPos, yPos, new Rook(PieceColor.WHITE));
        assertEquals(expectedSpot, actualSpot);
    }

    @Test
    void shouldBeAbleToGetWhitePawnFromASpotAtPositionOneZero() throws ChessBoardIndexOutOfBoundException {
        Board board = new Board();
        Pawn actualWhitePawn = new Pawn(PieceColor.WHITE);

        Piece expectedPawn = board.getSpotAt(1, 0).getPiece();

        assertEquals(expectedPawn, actualWhitePawn);
    }

    @Test
    void shouldAssertNullWhenAnEmptySpotIsCalled() throws ChessBoardIndexOutOfBoundException {
        Board board = new Board();

        Piece expectedPiece = board.getSpotAt(2, 0).getPiece();

        assertNull(expectedPiece);
    }
}
